<?php 
session_start();
$cursobasico = 2500;
$cursoavancado = 4500;
$cursogit = 500;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>TESTE BACKEND</title>
</head>
<body>
    <h1 id="cursos" >Cursos</h1>
                <!-- inicio modal --> 

            <!-- Button trigger modal -->
            <div class="form-group" id="mymodal">    
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Preços
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Preço dos cursos</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p><b> O curso Básico custa : <u><?php echo $cursobasico." R$"."<br>";?></u></b></p>
                        <p><b> O curso Avançado custa : <u><?php echo $cursoavancado." R$"."<br>";?></u></b></p>
                        <p><b> O curso Git custa : <u><?php echo $cursogit." R$"."<br>";?></u></b></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        
                    </div>
                    </div>
                </div>
                </div>
                </div>

            <!-- fim modal -->
            
    <form action="curso.php" method="post">
        <div id="formulario">    
        <select name="curso" id="curso" class="form-select">    
            <option value="basico" name="curso" id="bs">Curso programaçao Básico</option>
                <option value="avancado" name="curso" id="av">Curso programaçao Avançado </option>
                <option value="git" name="curso" class="gt">Curso programaçao Git </option>
            </select>

            <div class="form-group">
            <label > Nome :</label>
            <input type="text" name="nome" class="form-control" placeholder="Seu nome" required>
            </div>
            <div class="form-group">
            <label > Email :</label>
            <input type="email" name="email" class="form-control" placeholder="Seu email" required>
            </div>
            <div class="form-group">
            <label > Numero Cartão :</label>    
            <input type="text" name="cartaocredito" placeholder="0000 0000 0000 0000" class="form-control" required>
            </div>
            <div class="form-group">
            <label > Cvv :</label>    
             <input type="text" min="3" max="3" placeholder="123" name="cvv" class="form-control" required>
             </div>
             <div class="form-group">
             <label > Nome do Cartão :</label>
             <input type="text" name="nomecartao" class="form-control" placeholder="nome do cartão" required>
             </div>
             <div class="form-group">
             <label > Data de Vencimento :</label>     
             <input type="date" name="vencimento" class="form-control" required>
             </div>
             <div class="form-group">
             <label></label>
            <input type="submit" value="Comprar" class="form-control" id="btn" name="btn">
            </div>
            </div>
        </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="main.js"></script>
        
</body>
</html>